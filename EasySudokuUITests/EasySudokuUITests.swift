//
//  EasySudokuUITests.swift
//  EasySudokuUITests
//
//  Created by Alexey on 27/05/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import XCTest

class EasySudokuUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let app = XCUIApplication()
        app.launchEnvironment = ["isSnapshotMode": "YES"]
        setupSnapshot(app)
        app.launch()
        snapshot("01BoarsScreen")
        
        XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 4).children(matching: .button).element(boundBy: 6).tap()
        
        snapshot("01PickNumberScreen")
    }
    
}
