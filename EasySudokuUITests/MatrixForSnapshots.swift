//
//  MatrixForSnapshots.swift
//  Sudoku
//
//  Created by Alexey on 27/05/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

class MatrixForSnapshots {
    static let sharedInstance = MatrixForSnapshots()
    
    private init() {}
    
    let matrix: [[Int]] = [
        [7,6,4, 0,0,2, 0,1,3],
        [0,0,8, 1,3,0, 4,9,0],
        [9,0,0, 4,0,0, 7,5,2],
        
        [0,7,0, 8,2,5, 9,4,1],
        [0,9,1, 0,0,0, 0,7,5],
        [4,0,0, 0,0,1, 0,2,8],
        
        [1,4,0, 5,0,0, 3,6,9],
        [5,2,7, 3,6,0, 0,0,4],
        [0,3,9, 0,4,0, 5,8,7],
    ]
    
    let origin: [[Bool]] = [
        [true, true, true,      false,false,true,   false,true,false],
        [false,false,true,      true, true, false,  true, true,false],
        [true, false,false,     true, false,false,  true, true,false],
        
        [false,false,false,     true, true, true,   true, true, true],
        [false,true, true,      false,false,false,  false,true, true],
        [true, false,false,     false,false,true,   false,false,true],
        
        [true, true, false,     true, false,false,  true, true, true],
        [true, false,true,      true, true, false,  false,false,true],
        [false,true, true,      false,false,false,  true, true, true],
    ]
}
