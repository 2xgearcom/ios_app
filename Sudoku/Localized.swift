//
//  Localizable.swift
//  Sudoku
//
//  Created by Alexey on 26/05/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

class Localized {
    static var YouHaveSolvedTask = NSLocalizedString("You have successfully solved this task", comment: "")
    static var Congratulation = NSLocalizedString("Congratulation!", comment: "")
    static var OK = NSLocalizedString("OK", comment: "")
    static var Clear = NSLocalizedString("Clear", comment: "")
    static var StartNewGame = NSLocalizedString("Start New Game", comment: "")
}
