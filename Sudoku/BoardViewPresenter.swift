//
//  BoardViewPresenter.swift
//  Sudoku
//
//  Created by Alexey on 18/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import UIKit

protocol BoardViewPresenterDelegate: class {
    func updateValueForButton(value: Int, isOrigin: Bool, boxTag: Int, buttonTag: Int)
    func onGameFinish()
}

class BoardViewPresenter {
    weak var view: BoardViewDelegate?
    private let board: Board
    fileprivate let router: AppRouter
    
    fileprivate var boxPresenters = [Int: BoxPresenterDelegate]()
    
    init(board: Board, router: AppRouter) {
        self.board = board
        self.router = router
    }
    
    func registerBoxPresenter(presenter: BoxPresenterDelegate) {
        boxPresenters[presenter.tag] = presenter
    }
    
    func resetBoard() {
        board.reset()
    }
    
    func availableNumbers(boxTag: Int, buttonTag: Int) -> Set<Int> {
        return board.availableNumbers(boxTag: boxTag, buttonTag: buttonTag)
    }
    
    func onButtonTapped(boxTag: Int, buttonTag: Int, sourceView: UIView) {
        guard !board.isOrigin(boxTag: boxTag, buttonTag: buttonTag) else { return }
        guard let view = view else { return }

        router.showVariants(delegate: view, sourceView: sourceView, boxTag: boxTag, buttonTag: buttonTag)
    }
}

extension BoardViewPresenter: BoardViewPresenterDelegate {
    func updateValueForButton(value: Int, isOrigin: Bool, boxTag: Int, buttonTag: Int) {
        boxPresenters[boxTag]?.updateValueForButton(value: value, isOrigin: isOrigin, buttonTag: buttonTag)
    }
    
    func onGameFinish() {
        router.showSuccessAlert()
    }
}
