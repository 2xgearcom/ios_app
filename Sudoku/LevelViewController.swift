//
//  ViewController.swift
//  Sudoku
//
//  Created by Alexey on 16/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import UIKit

protocol LevelViewProtocol: class {

}

class LevelViewController: UIViewController {
    
    var presenter: LevelPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onEasyButtonTouchUpInside(_ sender: Any) {
        presenter.startWithLevel(level: .easy)
    }
    
    @IBAction func onEasyMediumTouchUpInside(_ sender: Any) {
        presenter.startWithLevel(level: .medium)
    }
    
    @IBAction func onEasyHardTouchUpInside(_ sender: Any) {
        presenter.startWithLevel(level: .hard)
    }

}

extension LevelViewController: LevelViewProtocol {
    
}
