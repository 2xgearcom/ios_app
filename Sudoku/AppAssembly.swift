//
//  AppAssembly.swift
//  Sudoku
//
//  Created by Alexey on 16/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import Swinject
import UIKit

class AppAssembly {
    
    static let sharedInstance = AppAssembly()
    
    private init() {}
    
    private let container = Container() { container in
        
        container.register(AppRouter.self) { resolver in
            AppRouter(resolver: resolver)
        }.inObjectScope(.container)
        
        container.register(LevelViewController.self) { resolver in
            let viewController = LevelViewController.createFromStoryboard() as! LevelViewController
            let appRouter = resolver.resolve(AppRouter.self)!
            let presenter = LevelPresenter(view: viewController, router: appRouter)
            viewController.presenter = presenter
            return viewController
        }.inObjectScope(.container)
        
        container.register(BoardViewController.self) { resolver in
            let viewController = BoardViewController.createFromStoryboard() as! BoardViewController
            let presenter = resolver.resolve(BoardViewPresenter.self)!
            presenter.view = viewController
            viewController.presenter = presenter
            return viewController
        }.inObjectScope(.container)
        
        container.register(BoardViewPresenter.self) { resolver in
            let board = resolver.resolve(Board.self)!
            let appRouter = resolver.resolve(AppRouter.self)!
            let presenter = BoardViewPresenter(board: board, router: appRouter)
            board.presenter = presenter
            return presenter
        }.inObjectScope(.container)
        
        container.register(BoxView.self) { (resolver, boxTag: Int) in
            let boardPresenter = resolver.resolve(BoardViewPresenter.self)!
            let presenter = BoxPresenter(boxTag: boxTag, boardPresenter: boardPresenter)
            let view = BoxView(presenter: presenter)
            presenter.view = view
            boardPresenter.registerBoxPresenter(presenter: presenter)
            return view
        }
        
        container.register(Board.self) { resolver in
            Board()
        }.inObjectScope(.container)
        
        container.register(VariantsViewController.self) { (resolver, boxTag: Int, buttonTag: Int, size: CGFloat) in
            let board = resolver.resolve(Board.self)!
            let presenter = VariantsPresenter(boxTag: boxTag, buttonTag: buttonTag, board: board)
            let viewController = VariantsViewController(size: size, presenter: presenter)
            presenter.view = viewController
            return viewController
        }
        
    }
    
    var appRouter: AppRouter {
        return container.resolve(AppRouter.self)!
    }
    
    func boxView(boxTag: Int) -> BoxView {
        return container.resolve(BoxView.self, argument: boxTag)!
    }
}
