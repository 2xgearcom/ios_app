//
//  LevelPresenter.swift
//  Sudoku
//
//  Created by Alexey on 16/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

enum ChosenLevel {
    case easy
    case medium
    case hard
}

class LevelPresenter {
    
    private weak var view: LevelViewProtocol?
    private var router: AppRouter
    
    init(view: LevelViewProtocol, router: AppRouter) {
        self.view = view
        self.router = router
    }
    
    func startWithLevel(level: ChosenLevel) {
        router.showBoard(level: level)
    }
}
