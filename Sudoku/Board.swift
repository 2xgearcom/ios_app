//
//  Board.swift
//  Sudoku
//
//  Created by Alexey on 18/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

class Board {
    
    static let smallRange = 0...2
    static let range = 0...8
    static let availableValues = 1...9
    
    private var matrix = [[Int]]()
    private var origin = [[Bool]]()
    
    private let boardGenerator = BoardGenerator()
    
    weak var presenter: BoardViewPresenterDelegate?
    
    func reset() {
        if ProcessInfo().environment["isSnapshotMode"] != nil {
            matrix = MatrixForSnapshots.sharedInstance.matrix
            origin = MatrixForSnapshots.sharedInstance.origin
        } else {
            matrix = boardGenerator.generateEasy()
            origin = matrix.map { $0.map { $0 != 0 } }
        }
        
        for y in Array(Board.range) {
            for x in Array(Board.range) {
                let value = matrix[y][x]
                let isOrigin = origin[y][x]
                let (boxTag, buttonTag) = Board.addressToTag(x: x, y: y)
                presenter?.updateValueForButton(value: value, isOrigin: isOrigin, boxTag: boxTag, buttonTag: buttonTag)
            }
        }
    }
    
    func isOrigin(boxTag: Int, buttonTag: Int) -> Bool {
        let (x, y) = Board.tagToAddress(boxTag: boxTag, buttonTag: buttonTag)

        return origin[y][x]
    }
    
    func availableNumbers(boxTag: Int, buttonTag: Int) -> Set<Int> {
        let (x, y) = Board.tagToAddress(boxTag: boxTag, buttonTag: buttonTag)
        
        return Board.availableNumbers(matrix: matrix, x: x, y: y)
    }
    
    func set(value: Int, boxTag: Int, buttonTag: Int) {
        let (x, y) = Board.tagToAddress(boxTag: boxTag, buttonTag: buttonTag)
        
        let available = Board.availableNumbers(matrix: matrix, x: x, y: y)
        
        guard available.contains(value) || value == 0 else { return }
        
        matrix[y][x] = value
        
        presenter?.updateValueForButton(value: value, isOrigin: false, boxTag: boxTag, buttonTag: buttonTag)
        
        if checkSuccess() {
            presenter?.onGameFinish()
        }
    }
    
    func checkSuccess() -> Bool {
        return matrix.reduce(true, { (res: Bool, row: [Int]) -> Bool in
            return res && !row.contains(0)
        })
    }
    
    class func smallAddressToTag(x: Int, y: Int) -> Int {
        return y * 3 + x
    }
    
    class func addressToTag(x: Int, y: Int) -> (boxTag: Int, buttonTag: Int) {
        let boxTag = Int(y / 3) * 3 + Int(x / 3)
        let buttonTag = (y % 3) * 3 + (x % 3)
        return (boxTag, buttonTag)
    }
    
    class func tagToSmallAddress(tag: Int) -> (x: Int, y: Int) {
        let y = Int(tag / 3)
        let x = tag - y * 3
        return (x, y)
    }
    
    class func tagToAddress(boxTag: Int, buttonTag: Int) -> (x: Int, y: Int) {
        let boxY = Int(boxTag / 3) * 3
        let buttonY = Int(buttonTag / 3)
        let y = boxY + buttonY
        let x = (boxTag - boxY) * 3 + buttonTag - buttonY * 3
        return (x,y)
    }
    
    class func availableNumbers(matrix: [[Int]], x: Int, y: Int) -> Set<Int> {
        let avInRow = Board.getAvailableFromRow(matrix: matrix, x: x, y: y)
        let avInCol = Board.getAvailableFromCol(matrix: matrix, x: x, y: y)
        let avInBox = Board.getAvailableFromBox(matrix: matrix, x: x, y: y)
        
        return avInRow.intersection(avInCol).intersection(avInBox)
    }
    
    private class func getAvailableFromRow(matrix: [[Int]], x: Int, y: Int) -> Set<Int> {
        let used = Set(matrix[y]).subtracting([matrix[y][x]])
        return Set(Board.availableValues).subtracting(used)
    }
    private class func getAvailableFromCol(matrix: [[Int]], x: Int, y: Int) -> Set<Int> {
        let used = Set(matrix.map { $0[x] } ).subtracting([matrix[y][x]])
        return Set(Board.availableValues).subtracting(used)
    }
    
    private class func getAvailableFromBox(matrix: [[Int]], x: Int, y: Int) -> Set<Int> {
        let startX = Int(x / 3) * 3
        let startY = Int(y / 3) * 3
        var used = [Int]()
        for i in startY..<startY + 3 {
            for j in startX..<startX + 3 {
                if i != y && j != x {
                    used.append(matrix[i][j])
                }
            }
        }
        return Set(Board.availableValues).subtracting(used)
    }
}


