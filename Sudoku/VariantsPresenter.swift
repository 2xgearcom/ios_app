//
//  VariantsPresenter.swift
//  Sudoku
//
//  Created by Alexey on 18/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

class VariantsPresenter {
    weak var view: VariantsViewDelegate?
    private let board: Board
    private let boxTag: Int
    private let buttonTag: Int
    
    init(boxTag: Int, buttonTag: Int, board: Board) {
        self.boxTag = boxTag
        self.buttonTag = buttonTag
        self.board = board
    }
    
    func viewDidLoad() {
        let numbers = board.availableNumbers(boxTag: boxTag, buttonTag: buttonTag)
        view?.prepareAvailableNumbers(numbers: numbers)
    }
    
    func onButtonTapped(value: Int) {
        board.set(value: value, boxTag: boxTag, buttonTag: buttonTag)
    }
}
