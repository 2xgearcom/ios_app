//
//  UIViewController.swift
//  amsterdam
//
//  Created by Alexey on 07/02/2017.
//  Copyright © 2017 capital. All rights reserved.
//

import UIKit

extension UIViewController {

    static func createFromStoryboard() -> UIViewController {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        guard let viewController = storyboard.instantiateInitialViewController() else {
            fatalError("unable to find view controller \(String(describing: self)) in XIB")
        }
        return viewController
    }
    
}
