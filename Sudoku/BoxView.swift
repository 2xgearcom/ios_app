//
//  BoxView.swift
//  Sudoku
//
//  Created by Alexey on 16/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import UIKit

protocol BoxViewDelegate {
    func updateCell(value: String, color: UIColor, x: Int, y: Int)
}

class BoxView: UIView {
    
    fileprivate(set) var buttons = [[UIButton]]()
    private let presenter: BoxPresenter
    
    init(presenter: BoxPresenter) {
        self.presenter = presenter
        super.init(frame: CGRect.zero)
        createCells()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createCells() {
        for y in Board.smallRange {
            var row = [UIButton]()
            for x in Board.smallRange {
                let button = UIButton()
                addSubview(button)
                
                button.setTitleColor(.black, for: .normal)
                button.tag = Board.smallAddressToTag(x: x, y: y)
                button.layer.cornerRadius = 3
                button.backgroundColor = UIColor(rgb: 0xEDF6FF)
                button.addTarget(self, action: #selector(onButtonTaped), for: .touchUpInside)
                row.append(button)
            }
            buttons.append(row)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        let buttonWidth = frame.width / 3
        let buttonHeight = frame.height / 3
        
        let fontSize = min(buttonWidth, buttonHeight) / 2
        let font = UIFont.systemFont(ofSize: fontSize)
        
        for y in Board.smallRange {
            for x in Board.smallRange {
                let buttonX = buttonWidth * CGFloat(x)
                let buttonY = buttonHeight * CGFloat(y)
                let button = buttons[y][x]
                button.frame = CGRect(x: buttonX + 0.5,
                                      y: buttonY + 0.5,
                                      width: buttonWidth - 1,
                                      height: buttonHeight - 1)
                button.titleLabel?.font = font
            }
        }
    }
    
    func onButtonTaped(sender: UIButton) {
        presenter.onButtonTapped(buttonView: sender)
    }

}

extension BoxView: BoxViewDelegate {
    func updateCell(value: String, color: UIColor, x: Int, y: Int) {
        let button = buttons[y][x]
        button.setTitle(value, for: .normal)
        button.setTitleColor(color, for: .normal)
    }
}
