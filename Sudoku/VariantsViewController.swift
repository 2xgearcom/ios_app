//
//  VariantsViewController.swift
//  Sudoku
//
//  Created by Alexey on 18/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import UIKit

protocol VariantsViewDelegate: class {
    func prepareAvailableNumbers(numbers: Set<Int>)
}

class VariantsViewController: UIViewController {
    
    let presenter: VariantsPresenter
    
    fileprivate var buttons = [[UIButton]]()
    private let clearButton = UIButton()
    private let contentView = UIView()
    private let viewWidth: CGFloat
    private let viewHeight: CGFloat
    private let viewPadding: CGFloat = 10
    
    init(size: CGFloat, presenter: VariantsPresenter) {
        self.viewWidth = size + viewPadding * 2
        self.viewHeight = viewWidth + viewWidth / 3
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferredContentSize = CGSize(width: viewWidth, height: viewHeight)
        
        popoverPresentationController?.backgroundColor = .clear
        
        contentView.frame = CGRect(x: viewPadding,
                                   y: viewPadding,
                                   width: viewWidth - (viewPadding * 2),
                                   height: viewHeight - (viewPadding * 2))
        view.addSubview(contentView)
        
        let buttonColor = UIColor(rgb: 0xEDF6FF)
        let buttonTextColor = UIColor(rgb: 0x007AFF)//UIColor(rgb: 0x35ce3c)
        
        for y in Board.smallRange {
            var row = [UIButton]()
            for x in Board.smallRange {
                let button = UIButton()
                button.tag = Board.smallAddressToTag(x: x, y: y)
                button.layer.cornerRadius = 3
                button.backgroundColor = buttonColor
                button.setTitleColor(buttonTextColor, for: .normal)
                button.addTarget(self, action: #selector(onButtonTapped), for: .touchUpInside)
                
                row.append(button)
                contentView.addSubview(button)
            }
            buttons.append(row)
        }
        
        clearButton.setTitle(Localized.Clear, for: .normal)
        clearButton.backgroundColor = buttonColor
        clearButton.setTitleColor(buttonTextColor, for: .normal)
        clearButton.layer.cornerRadius = 3
        clearButton.addTarget(self, action: #selector(onClearButtonTapped), for: .touchUpInside)
        contentView.addSubview(clearButton)
        
        presenter.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let buttonSize = contentView.frame.width / 3
        let fontSize = buttonSize / 2
        let font = UIFont.systemFont(ofSize: fontSize)
        
        clearButton.titleLabel?.font = font
        
        for y in Board.smallRange {
            for x in Board.smallRange {
                let buttonX = buttonSize * CGFloat(x)
                let buttonY = buttonSize * CGFloat(y)
                let button = buttons[y][x]
                button.frame = CGRect(x: buttonX + 0.5,
                                      y: buttonY + 0.5,
                                      width: buttonSize - 1,
                                      height: buttonSize - 1)
                
                button.titleLabel?.font = font
            }
        }
        
        clearButton.frame = CGRect(x: 0,
                                   y: contentView.frame.width + 0.5,
                                   width: contentView.frame.width,
                                   height: buttonSize - 1)
    }
    
    func onButtonTapped(sender: UIButton) {
        dismiss(animated: false, completion: nil)
        let number = sender.tag + 1
        presenter.onButtonTapped(value: number)
    }
    
    func onClearButtonTapped(sender: UIButton) {
        dismiss(animated: false, completion: nil)
        presenter.onButtonTapped(value: 0)
    }
}

extension VariantsViewController: VariantsViewDelegate {
    func prepareAvailableNumbers(numbers: Set<Int>) {
        for row in buttons {
            for button in row {
                let buttonNumber = button.tag + 1
                if numbers.contains(buttonNumber) {
                    button.setTitle("\(buttonNumber)", for: .normal)
                } else {
                    button.isEnabled = false
                    button.isUserInteractionEnabled = false
                }
            }
        }
    }
    
}
