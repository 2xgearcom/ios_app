//
//  AppRouter.swift
//  Sudoku
//
//  Created by Alexey on 17/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import UIKit
import Swinject

class AppRouter {
    
    let mainNavigationController = UINavigationController()
    fileprivate let resolver: Resolver
    
    init(resolver: Resolver) {
        self.resolver = resolver
        mainNavigationController.setNavigationBarHidden(true, animated: false)
    }
    
    func showLevelVariants() {
        mainNavigationController.pushViewController(levelViewController, animated: false)
    }
    
    func showBoard(level: ChosenLevel) {
        boardViewController.level = level
        mainNavigationController.pushViewController(boardViewController, animated: false)
        
    }
    
    func showVariants(delegate: UIPopoverPresentationControllerDelegate, sourceView: UIView, boxTag: Int, buttonTag: Int) {
        let viewController = resolver.resolve(VariantsViewController.self, arguments: boxTag, buttonTag, boardViewController.boxSize)!
        viewController.modalPresentationStyle = .popover
        let popover = viewController.popoverPresentationController
        popover?.delegate = delegate
        popover?.sourceView = sourceView
        popover?.sourceRect = sourceView.bounds
        boardViewController.present(viewController, animated: true, completion: nil)
    }
    
    func showSuccessAlert() {
        let alertController = UIAlertController(title: Localized.Congratulation, message: Localized.YouHaveSolvedTask, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: Localized.OK, style: .cancel, handler: nil)
        
        alertController.addAction(closeAction)
        
        boardViewController.present(alertController, animated: true, completion: nil)
    }
    
}

extension AppRouter {
    var levelViewController: LevelViewController {
        return resolver.resolve(LevelViewController.self)!
    }
    var boardViewController: BoardViewController {
        return resolver.resolve(BoardViewController.self)!
    }
}
