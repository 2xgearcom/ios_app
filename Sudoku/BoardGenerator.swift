//
//  BoardGenerator.swift
//  Sudoku
//
//  Created by Alexey on 29/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation

class BoardGenerator {
    private static let matrixRange = 0..<9
    private static let shortMatrixRange = 0..<3
    
    private var matrix = [[Int]]()
    
    init() {
        resetMatrix()
    }
    
    private func resetMatrix() {
        matrix.removeAll()
        for _ in BoardGenerator.matrixRange {
            matrix.append(Array(repeating: 0, count: 9))
        }
    }
    
    class func shuffledArray(set: Set<Int>) -> [Int] {
        var numbers = Array(set)
        var rndNumbers = [Int]()
        for _ in numbers {
            let rndIdx = Int(arc4random_uniform(UInt32(numbers.count)))
            rndNumbers.append(numbers[rndIdx])
            numbers.remove(at: rndIdx)
        }
        return rndNumbers
    }
    
    private func setPrimaryNumbers() {
        for i in BoardGenerator.shortMatrixRange {
            for j in BoardGenerator.shortMatrixRange {
                let shaffled = BoardGenerator.shuffledArray(set: Set(1...9))
                let startY = i * 3
                let startX = j * 3
                var idx = 0
                for y in startY...startY + 2 {
                    for x in startX...startX + 2 {
                        matrix[y][x] = shaffled[idx]
                        idx += 1
                    }
                }
            }
        }
    }
    
    private func revolveLines() -> Bool {
        
        var conflictCounter = 0
        
        for i in BoardGenerator.shortMatrixRange {
            let startY = i * 3
            var line_1 = matrix[startY]
            var line_2 = matrix[startY + 1]
            var line_3 = matrix[startY + 2]
            
            var resolved = [Int]()
            
            var x = 0
            var revolve = 0
            while x < 9 {
                if !resolved.contains(line_1[x]) {
                    resolved.append(line_1[x])
                    x += 1
                    revolve = 0
                } else {
                    let conflictNumber = line_1[x]
                    line_1[x] = line_2[x]
                    line_2[x] = line_3[x]
                    line_3[x] = conflictNumber
                    
                    revolve += 1
                    if revolve == 3 {
                        revolve = 1
                        conflictCounter += 1
                        if conflictCounter == 100 { return false }
                        
                        var buffer = line_1[x]
                        line_1[x] = line_2[x]
                        line_2[x] = line_3[x]
                        line_3[x] = buffer
                        
                        let conflictVal = line_1[x]
                        
                        x = resolved.index(of: conflictVal)!
                        resolved.removeLast(resolved.count - x)
                        
                        buffer = line_1[x]
                        line_1[x] = line_2[x]
                        line_2[x] = line_3[x]
                        line_3[x] = buffer
                    }
                }
            }
            
            conflictCounter = 0
            
            resolved.removeAll()
            x = 0
            revolve = 0
            while x < 9 {
                if !resolved.contains(line_2[x]) {
                    resolved.append(line_2[x])
                    x += 1
                    revolve = 0
                } else {
                    let conflictNumber = line_2[x]
                    line_2[x] = line_3[x]
                    line_3[x] = conflictNumber
                    
                    revolve += 1
                    if revolve == 2 {
                        revolve = 1
                        conflictCounter += 1
                        if conflictCounter == 100 { return false }
                        
                        var buffer = line_2[x]
                        line_2[x] = line_3[x]
                        line_3[x] = buffer
                        
                        let conflictVal = line_2[x]
                        
                        x = resolved.index(of: conflictVal)!
                        resolved.removeLast(resolved.count - x)
                        
                        buffer = line_2[x]
                        line_2[x] = line_3[x]
                        line_3[x] = buffer
                    }
                }
            }
            
            matrix[startY] = line_1
            matrix[startY + 1] = line_2
            matrix[startY + 2] = line_3
        }
        
        return true
    }
    
    private func flipMatrix() {
        for y in BoardGenerator.matrixRange {
            for x in (y + 1)..<9 {
                let swap = matrix[y][x]
                matrix[y][x] = matrix[x][y]
                matrix[x][y] = swap
            }
        }
    }
        
    private func generate() -> [[Int]] {
        while true {
            resetMatrix()
            setPrimaryNumbers()
            if !revolveLines() { continue }
            flipMatrix()
            if !revolveLines() { continue }
            break
        }
        return matrix
    }
    
    func generateEasy() -> [[Int]] {
        var matrix = generate()
        
        let idxArr = BoardGenerator.shuffledArray(set: Set(0..<81))
        for idx in idxArr {
            let y = Int(idx / 9)
            let x = idx - y * 9
            
            let available = Board.availableNumbers(matrix: matrix, x: x, y: y)
            if available.count == 1 {
                matrix[y][x] = 0
            }
        }
        //matrix[7][7] = 0
        //matrix[8][7] = 0
        
        return matrix
    }
    
    func generateMedium() -> [[Int]] {
        let matrix = generate()
        
        return matrix
    }
    
    func generateHard() -> [[Int]] {
        let matrix = generate()
        
        return matrix
    }
}
