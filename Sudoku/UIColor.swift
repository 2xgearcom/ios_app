//
//  UIColor.swift
//  Sudoku
//
//  Created by Alexey on 18/05/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience public init(argb: UInt) {
        let alpha = CGFloat(argb >> 0x18) / 255
        let red = CGFloat((argb & 0xff0000) >> 0x10) / 255
        let green = CGFloat((argb & 0xff00) >> 0x8) / 255
        let blue = CGFloat(argb & 0xff) / 255
        /*if #available(iOS 10.0, *) {
            self.init(displayP3Red: red, green: green, blue: blue, alpha: alpha)
        } else {*/
            self.init(red: red, green: green, blue: blue, alpha: alpha)
        //}
    }
    
    convenience public init(rgb: UInt) {
        self.init(argb: rgb | 0xff000000)
    }
    
}
