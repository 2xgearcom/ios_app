//
//  BoardViewController.swift
//  Sudoku
//
//  Created by Alexey on 16/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import UIKit
import Swinject

protocol BoardViewDelegate: UIPopoverPresentationControllerDelegate {
    
}

class BoardViewController: UIViewController {

    @IBOutlet weak var boardView: UIView!
    @IBOutlet weak var newGameButton: UIButton!
    
    var level: ChosenLevel?
    var presenter: BoardViewPresenter?
    let appAssembly = AppAssembly.sharedInstance
    var boxSize: CGFloat = 0
    
    fileprivate var boxes = [[BoxView]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        boardView.layer.cornerRadius = 5
        boardView.backgroundColor = UIColor(argb: 0x44000000)
        
        for y in Board.smallRange {
            var row = [BoxView]()
            for x in Board.smallRange {
                let boxTag = Board.smallAddressToTag(x: x, y: y)
                let boxView = appAssembly.boxView(boxTag: boxTag)
                row.append(boxView)
                boardView.addSubview(boxView)
            }
            boxes.append(row)
        }
        
        newGameButton.addTarget(self, action: #selector(startNewGame), for: .touchUpInside)
        newGameButton.setTitle(Localized.StartNewGame, for: .normal)
        
        presenter?.resetBoard()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let boxWidth = boardView.bounds.width / 3
        let boxHeight = boardView.bounds.height / 3
        for y in Board.smallRange {
            for x in Board.smallRange {
                let boxX = boxWidth * CGFloat(x)
                let boxY = boxHeight * CGFloat(y)
                let frame = CGRect(x: boxX + 1,
                                   y: boxY + 1,
                                   width: boxWidth - 2,
                                   height: boxHeight - 2)
                let box = boxes[y][x]
                box.frame = frame
                
            }
        }
        
        boxSize = boxWidth
    }

    @objc private func startNewGame() {
        presenter?.resetBoard()
    }

}

extension BoardViewController: BoardViewDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

