//
//  BoxPresenter.swift
//  Sudoku
//
//  Created by Alexey on 18/04/2017.
//  Copyright © 2017 2xGear. All rights reserved.
//

import Foundation
import UIKit

protocol BoxPresenterDelegate {
    var tag: Int { get }
    func updateValueForButton(value: Int, isOrigin: Bool, buttonTag: Int)
}

class BoxPresenter {
    weak var view: BoxView?
    let boardPresenter: BoardViewPresenter
    let boxTag: Int
    
    init(boxTag: Int, boardPresenter: BoardViewPresenter) {
        self.boxTag = boxTag
        self.boardPresenter = boardPresenter
    }
    
    func onButtonTapped(buttonView: UIView) {
        boardPresenter.onButtonTapped(boxTag: boxTag, buttonTag: buttonView.tag, sourceView: buttonView)
    }
}

extension BoxPresenter: BoxPresenterDelegate {
    var tag: Int {
        return boxTag
    }
    
    func updateValueForButton(value: Int, isOrigin: Bool, buttonTag: Int) {
        let strVal = (value != 0) ? "\(value)" : ""
        let color: UIColor = isOrigin ? .black : UIColor(rgb: 0x007AFF)
        let (x, y) = Board.tagToSmallAddress(tag: buttonTag)
        view?.updateCell(value: strVal, color: color, x: x, y: y)
    }
}
